## Músicas prontas ou em ensaio

* Breathe - Pink Floyd
* Computadores fazem arte - Nação Zumbi
* Polícia - Titãs
* Tropicália - Caetano
* Cérebro eletrônico - Gilberto Gil
* Acima do bem e do mal - Maria Sabina
* Abalo sísmico - Maria Sabina
* Santa Tecnologia - Masia Sabina

## Músicas pendentes de ensaio

* 1990 quadrillions de tonnes
* Living la vida loca
* Eu você e os garçons
* Tempo arruaceiro
* Guerreiros Reluzentes
