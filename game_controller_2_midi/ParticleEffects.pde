// A simple Particle class

int rcf() { return (int) random(255); }
color rc() { return color(rcf(),rcf(),rcf()); }

class Particle {
  PVector position;
  PVector velocity;
  PVector acceleration;
  float lifespan;
  color col;
  
  Particle(PVector l, color c) {
    acceleration = new PVector(0, 0);
    velocity = new PVector(random(-2, 2), random(-2, 2));
    position = l.copy();
    lifespan = 255.0;
    col = c;   
  }

  void run() {
    update();
    draw();
  }

  // Method to update position
  void update() {
    velocity.add(acceleration);
    position.add(velocity);
    lifespan -= 1.0;
  }

  // Method to display
  void draw() {
    stroke(col, lifespan);
    fill(col, lifespan);
    ellipse(position.x, position.y, 8, 8);
  }

  // Is the particle still useful?
  boolean isDead() {
    if (lifespan < 0.0) {
      return true;
    } else {
      return false;
    }
  }
}

// A class to describe a group of Particles
// An ArrayList is used to manage the list of Particles 

class ParticleSystem {
  ArrayList<Particle> particles;

  ParticleSystem() {
    particles = new ArrayList<Particle>();
  }

  void addParticle(int x, int y, color c) {
    particles.add(new Particle(new PVector(x,y), c));
  }

  void run() {
    for (int i = particles.size()-1; i >= 0; i--) {
      Particle p = particles.get(i);
      p.run();
      if (p.isDead()) {
        particles.remove(i);
      }
    }
  }
}
