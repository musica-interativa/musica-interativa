// Agents link multiple controls together into something that grabs input from a controller,
// draws, and sends to MIDI output

// agent can have multiple controllers involved in same overall task eg. joystick and a trigger


interface IAgent {

  void updateFrom(ControlDevice cd);  
  void draw();
  void play(IPlayerProxy p);
}


class NullAgent implements IAgent {
   void updateFrom(ControlDevice cd) { }
   void draw() { }
   void play(IPlayerProxy p) { }
}

class AgentCollection implements IAgent {
  
  ArrayList<IAgent> agents;
  
  AgentCollection() {
    agents = new ArrayList<IAgent>();
  }
  
  void add(IAgent a) {
    agents.add(a);
  }
  
  void updateFrom(ControlDevice cd) {
    for (IAgent a : agents) {
      a.updateFrom(cd);
    }
  }
  
  void draw() {
    for (IAgent a : agents) {
      a.draw();
    }    
  }
  
  void play(IPlayerProxy p) {
    for (IAgent a : agents) {
      a.play(p);
    }
  }
}




class ToggleVolume implements IControlProxy {
  ToggleSwitch ts;
  String controllerParamId;
  
  int vol = 0;
  int channel, param;
  
  ToggleVolume(String paramId, int chan, int p, int x, int y, color c) {
    channel = chan;
    param = p;
    controllerParamId = paramId;
    ts = new ToggleSwitch(controllerParamId,x,y,c);
  }

  
  void set(boolean b) {
    ts.set(b);
  }
  
  boolean toggledOn() { return ts.toggledOn(); }
  boolean toggledOff() { return ts.toggledOff(); }
  
  void play(IPlayerProxy myInstrument) {
    if (ts.toggledOn()) {
      vol = 100-vol;
      myInstrument.sendCC(channel, param, vol);
    }

    if (ts.toggledOff()) {
      vol = 100-vol;
      myInstrument.sendCC(channel, param, vol);      
    }
    ts.reset();
    throttler.step();
  }
  
  void draw(int x, int y, color fillCol, color strokeCol) {
    pushStyle();
    fill(fillCol);
    stroke(strokeCol);
    ellipse(x,y,30,30);
    popStyle();
  }
  
  void updateFrom(ControlDevice cd) {
    set(gpad.getButton(controllerParamId).pressed());
  }

}
