--[[
name: midi constrainer
description: Define a scale in the lowest octave (0), and this plugin filters out notes which aren't in it
author: Phil Jones / Synaesmedia
--]]

require "include/protoplug"

local blockEvents = {}

local allowed_notes = {"_","_","_","_", "_","_","_","_", "_","_","_","_",}

function mod(a, b)
    return a - (math.floor(a/b)*b)
end

function allowed_str()
   s = "allowed: "
   for i = 1,12,1 do
   	 s = s..allowed_notes[i]
   end
   return s
end

function n2i(n) 
	return mod(n,12)+1
end

function allow(n) 
	print("allowing " .. n)				
	allowed_notes[n2i(n)]="X"
end

function disallow(n)
	print("dis-allowing " .. n)				
	allowed_notes[n2i(n)]="_"
end

function allowed(n)
	return allowed_notes[n2i(n)] == "X"
end


function plugin.processBlock(samples, smax, midiBuf)
	blockEvents = {}
	-- print allowed notes
		
	for ev in midiBuf:eachEvent() do
	
		if ev:isNoteOn() then
			local n = ev:getNote()
			local old_n = n
			print("BBB "..n)
			if n < 12 then
				allow(n)
				print(allowed_str())
			else
				flag = true
				print(ev)
				while ( flag ) do
					if (allowed(n)) then
						flag = false
						
						local newEv = midi.Event.noteOn(
							ev:getChannel(), 
							n, 
							ev:getVel())
						table.insert(blockEvents, newEv)						
					end
					n=n+1
					if (n > 200) then 
						flag = false
						print("FAILED "..old_n..", "..n)
					end						
				end
			end
			
		elseif ev:isNoteOff() then

			local n = ev:getNote()
			if n < 12 then
				disallow(n)
			else
				flag = true
				
				while ( flag ) do
					if allowed(n) then
					    print("OK "..n)
						flag = false
						local newEv = midi.Event.noteOff(
							ev:getChannel(), 
							n, 
							ev:getVel())
						table.insert(blockEvents, newEv)
						
					end
					n=n+1

					if (n > 200) then 
						flag = false
						print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
					end						
				end
			end

		end	
	end
	-- fill midi buffer with prepared notes
	midiBuf:clear()
	if #blockEvents>0 then
		for _,e in ipairs(blockEvents) do
			if (e:isNoteOn()) then
				print("Note On")
			end
			if (e:isNoteOff()) then
				print("Note Off")
			end
		    print(e:getChannel()..","..e:getNote()..","..e:getVel())
			midiBuf:addEvent(e)
		end
	end
end

