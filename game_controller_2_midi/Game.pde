interface IGame {
  ImageCollection getBackgrounds();
  AgentCollection getAgents();
   
}

class Game implements IGame {
  ImageCollection imageCollection = new ImageCollection();
  AgentCollection agentCollection = new AgentCollection();
  
  void addBG(String imgName, float startScaler) {
    imageCollection.add(imgName,startScaler);
  }
 
  void addAgent( IAgent a) {
     agentCollection.add(a);
  } 
  
  AgentCollection getAgents() { return agentCollection; }
  ImageCollection getBackgrounds() { return imageCollection; }
}


Game makeTestGame() {
  Game g = new Game();
  
  g.addBG("bg6.png",0.1);
  int i = (int)random(7);
  g.addBG("bg"+i+".png",0.001);
  g.addBG("bg"+i+".jpg",0.00001);
  i = (int)random(15)+1;
  g.addBG("bg"+i+".jpg",0.0000001);  
  
  g.addAgent(new CardinalDrumsAgent("B_NORTH","B_EAST","B_SOUTH","B_WEST"));
  g.addAgent(new StableJoystickNotesAgent ("LEFT_X","LEFT_Y",   "TRIG3", "TRIG1", color(0,0,255,100), 25, 5));
  g.addAgent(new SimpleJoystickNotesAgent("RIGHT_X","RIGHT_Y", "TRIG2", color(255,0,100,100), 20, 2, 50, 150));
  return g;
}
