class SimpleJoystickCCAgent implements IAgent {
    // This is for direct control from joystick to 2 parameters
    SliderPairControl joy;
    ButtonControl button;
    
    color col;
    int midiChannel;
    int midiP1, midiP2;
    
    SimpleJoystickCCAgent(String xParamName, String yParamName, String bParamName, color c, int chan, int mp1, int mp2, int x, int y) {
       joy = new SliderPairControl(xParamName, yParamName);
       button = new ButtonControl(bParamName,x,y,c);
       col = c;
       midiChannel = chan;
       midiP1 = mp1;
       midiP2 = mp2;
    }
    
    void updateFrom(ControlDevice gpad) {
      joy.updateFrom(gpad);
      button.updateFrom(gpad);
    }
    
    void draw() {
      pushStyle();
        fill(col);
        ellipse(joy.getXfor(0,width),joy.getYfor(0,height),20,20);
        int w=40;
        int h=30;
        if (button.down) {
          stroke(255,200,200);
          w = 50;
        } else {
          stroke(100,50,50);
        }
        rect(button.x,button.y,w,h);
      popStyle();
    }

    void play(IPlayerProxy myInstrument) {
      if (button.isDown()) {
        myInstrument.sendCC(midiChannel,midiP1, (int) joy.getXfor(0,127) );  
        myInstrument.sendCC(midiChannel,midiP2, (int) joy.getYfor(0,127) );
      }   
    }
}


class SimpleJoystickNotesAgent implements IAgent {
    // This is for direct control from joystick to 2 parameters
    SliderPairControl joy;
    ButtonControl button;
    ParticleSystem ps;
        
    color col;
    int midiChannel;
    int radius;
    float a,da;
    
    SimpleJoystickNotesAgent(String xParamName, String yParamName, String bParamName, color c, int rad, int chan, int x, int y) {
       joy = new SliderPairControl(xParamName, yParamName);
       button = new ButtonControl(bParamName,x,y,c);
       col = c;
       midiChannel = chan;
       radius = rad;
       a = 0;
       ps = new ParticleSystem();
    }
    
    void updateFrom(ControlDevice gpad) {
      joy.updateFrom(gpad);
      button.updateFrom(gpad);
    }
    
    void draw() {
      pushStyle();
        fill(col);
        stroke(20,20,80);
        pushMatrix();
          translate(joy.getXfor(0,width),joy.getYfor(0,height));
          float da2 = 2*PI/7;
          float tr;
          if (button.down) { 
            tr = radius*1.5;
            da = 0.03;
          } else { 
            tr = radius; 
            da = 0.01;
          }
          a=a+da;
          rotate(a);
  
          for (int i=0;i<7;i++) {
            rotate(da2);       
            ellipse(0,tr,radius*0.8,radius*0.8);                   
          }
        popMatrix();
        
        int w=40;
        int h=30;
        if (button.down) {
          stroke(255,200,200);
          ps.addParticle((int)joy.getXfor(0,width),(int)joy.getYfor(0,height),color(col));
          w = 50;
        } else {
          stroke(100,50,50);
        }
        ps.run();

        rect(button.x,button.y,w,h);
      popStyle();
    }

    void play(IPlayerProxy myInstrument) {
      if (button.isPressed()) {
        myInstrument.sendNote(midiChannel,(int) joy.getYfor(88,36), (int) joy.getXfor(64,127) );  
        //button.set(false);
      }   
    }
  
}

////////////


class StableJoystickNotesAgent implements IAgent {
    // This is for direct control from joystick to 2 parameters
    SliderPairControl joy;
    ButtonControl moveButton;
    ButtonControl fireButton;
    ParticleSystem ps;
    
    color col;
    int midiChannel;
    int radius;
    float a,da;
    int x, y;
    
    StableJoystickNotesAgent(String xParamName, String yParamName, String bMoveName, String bFireName, color c, int rad, int chan) {
       x = width/2;
       y = height/2;
       joy = new SliderPairControl(xParamName, yParamName);
       moveButton = new ButtonControl(bMoveName,width - 100,100,c);
       fireButton = new ButtonControl(bFireName,width - 200,100,c);
       col = c;
       midiChannel = chan;
       radius = rad;
       a = 0;
       ps = new ParticleSystem();

   }
    
    void updateFrom(ControlDevice gpad) {
      joy.updateFrom(gpad);
      moveButton.updateFrom(gpad);
      fireButton.updateFrom(gpad);
      if (moveButton.down) {
        x = (int)joy.getXfor(0,width);
        y = (int)joy.getYfor(0,height);
      }
    }
    
    void draw() {
      pushStyle();
        fill(col);
        stroke(20,20,80);
        pushMatrix();
          translate(x,y);
          float da2 = 2*PI/7;
          float tr;
          if (fireButton.down) { 
            tr = radius*1.5;
            da = 0.03;
          } else { 
            tr = radius; 
            da = 0.01;
          }
          a=a+da;
          rotate(a);
  
          for (int i=0;i<7;i++) {
            rotate(da2);       
            ellipse(0,tr,radius*0.8,radius*0.8);                   
          }
        popMatrix();


        
        int w=40;
        int h=30;
        if (fireButton.down) {
          stroke(255,200,200);
          w = 50;
          ps.addParticle(x,y,color(255));

        } else {
          stroke(100,50,50);
        }
        rect(moveButton.x,moveButton.y,w,h);
        rect(fireButton.x,fireButton.y,w,h);
        
        ps.run();

        
      popStyle();
    }

    void play(IPlayerProxy myInstrument) {
      if (fireButton.isPressed()) {
        myInstrument.sendNote(midiChannel,(int) map(y,0,height,88,36), (int)map(x,0,width,64,127) );  
      }   
    }
  
}



////////////

class HatNotesAgent implements IAgent {
    // This is for direct control from joystick to 2 parameters
    HatControl hat;
    ButtonControl button;
    
    color col;
    int midiChannel;
    int radius;
    float a,da;
    int x, y;
    int speed;
    
    HatNotesAgent(String bParamName, color c, int rad, int chan) {
       hat = new HatControl();
       button = new ButtonControl(bParamName,x,y,c);
       col = c;
       midiChannel = chan;
       radius = rad;
       a = 0;
       x = width/2;
       y = width/2;
       speed = 10;
    }
    
    void updateFrom(ControlDevice gpad) {
      hat.updateFrom(gpad);
      switch (hat.getDirection()) {
        case NORTH :
          y = y - speed;
          break;
        case EAST :
          x = x + speed;
          break;
        case SOUTH :
          y = y + speed;
          break;
        case WEST :  
          x = x - speed;
          break;
        default :
       
      }
      button.updateFrom(gpad);
    }
    
    float getXfor(float min, float max) {
      return map(x, 0, width, min, max);
    }
    
    float getYfor(float min, float max) {
      return map(y, 0, height, min, max);
    }
    
    void draw() {
      pushStyle();
        fill(col);
        stroke(20,20,80);
        pushMatrix();
          translate(getXfor(0.0,width),getYfor(0.0,height));
          float da2 = 2*PI/7;
          float tr;
          if (button.down) { 
            tr = radius*1.5;
            da = 0.03;
          } else { 
            tr = radius; 
            da = 0.01;
          }
          a=a+da;
          rotate(a);
  
          for (int i=0;i<7;i++) {
            rotate(da2);       
            ellipse(0,tr,radius*0.8,radius*0.8);                   
          }
        popMatrix();
        
        int w=40;
        int h=30;
        if (button.down) {
          stroke(255,200,200);
          w = 50;
        } else {
          stroke(100,50,50);
        }
        rect(button.x,button.y,w,h);
      popStyle();
    }

    void play(IPlayerProxy myInstrument) {
      if (button.isDown()) {
        myInstrument.sendNote(midiChannel,(int) getYfor(88,36), (int) getXfor(64,127) );  
        button.set(false);
      }   
    }
  
}
