class SimpleHatNotesAgent implements IAgent {
    // This is for direct control from hat to 2 parameters
    SliderPairControl joy;
    ButtonControl button;
    
    color col;
    int midiChannel;
    int radius;
    float a,da;
    
    SimpleHatNotesAgent(String xParamName, String yParamName, String bParamName, color c, int rad, int chan, int x, int y) {
       joy = new SliderPairControl(xParamName, yParamName);
       button = new ButtonControl(bParamName,x,y,c);
       col = c;
       midiChannel = chan;
       radius = rad;
       a = 0;
    }
    
    void updateFrom(ControlDevice gpad) {
      joy.updateFrom(gpad);
      button.updateFrom(gpad);
    }
    
    void draw() {
      pushStyle();
        fill(col);
        stroke(20,20,80);
        pushMatrix();
          translate(joy.getXfor(0,width),joy.getYfor(0,height));
          float da2 = 2*PI/7;
          float tr;
          if (button.down) { 
            tr = radius*1.5;
            da = 0.03;
          } else { 
            tr = radius; 
            da = 0.01;
          }
          a=a+da;
          rotate(a);
  
          for (int i=0;i<7;i++) {
            rotate(da2);       
            ellipse(0,tr,radius*0.8,radius*0.8);                   
          }
        popMatrix();
        
        int w=40;
        int h=30;
        if (button.down) {
          stroke(255,200,200);
          w = 50;
        } else {
          stroke(100,50,50);
        }
        rect(button.x,button.y,w,h);
      popStyle();
    }

    void play(IPlayerProxy myInstrument) {
      if (button.isDown()) {
        myInstrument.sendNote(midiChannel,(int) joy.getYfor(88,36), (int) joy.getXfor(64,127) );  
        button.set(false);
      }   
    }
  
}



class CardinalTogglesAgent implements IAgent {
    ToggleVolume north, east, south, west; 

    CardinalTogglesAgent(String nParam, String eParam, String sParam, String wParam) {    
      north = new ToggleVolume(nParam,2,1,100,100,color(0,0,255,200));
      east = new ToggleVolume(eParam,2,2,150,100,color(0,255,0,200));
      south = new ToggleVolume(sParam,2,3,200,100,color(255,0,0,200));
      west = new ToggleVolume(wParam,2,4,250,100,color(200,200,100,200));
    }
  
    void updateFrom(ControlDevice gpad) {
      north.updateFrom(gpad);
      east.updateFrom(gpad);
      south.updateFrom(gpad);
      west.updateFrom(gpad);
    }
    
    void play(IPlayerProxy myInstrument) {
      north.play(myInstrument);
      east.play(myInstrument);
      south.play(myInstrument);
      west.play(myInstrument);
    }
    
    void draw() {
      pushMatrix();
        translate(width/2,height/2);
        color fc = color(100,200,255);
        color fc2 = color(200,240,255);
        color sc = color(255);
        
        north.draw(0,-80, north.toggledOn()?fc:fc2,sc);
        east.draw(80,0,east.toggledOn()?fc:fc2,sc);
        south.draw(0,80,south.toggledOn()?fc:fc2,sc);
        west.draw(-80,0,west.toggledOn()?fc:fc2,sc);
      popMatrix();
    }
}

class SingleNoteControlProxy implements IControlProxy {
  String param;
  int channel;
  int note;
  boolean down;
  color col;
  
  SingleNoteControlProxy(String p, int chan, int n, color c) {
    param = p;
    channel = chan;
    note = n;
    col = c;
  }
  
  void updateFrom(ControlDevice gpad) {
      down = gpad.getButton(param).pressed();
  }
  
  void play(IPlayerProxy player) {
    if (down) {
       player.sendNote(channel, 60, 127);  
    }
  }
  
  boolean isOn() { return down; }
  
  void draw(int x, int y, color fillCol, color strokeCol) {
    pushStyle();
    fill(fillCol);
    stroke(strokeCol);
    ellipse(x,y,30,30);
    popStyle();
  }
}

class CardinalDrumsAgent implements IAgent {
    SingleNoteControlProxy north, east, south, west; 
    ParticleSystem ps;

    CardinalDrumsAgent(String nParam, String eParam, String sParam, String wParam) {    
      north = new SingleNoteControlProxy(nParam,2,60,color(0,0,255,200));
      east = new SingleNoteControlProxy(eParam,2,60,color(0,255,0,200));
      south = new SingleNoteControlProxy(sParam,2,60,color(255,0,0,200));
      west = new SingleNoteControlProxy(wParam,2,60,color(200,200,100,200));
      ps = new ParticleSystem();
    }
  
    void updateFrom(ControlDevice gpad) {
      north.updateFrom(gpad);
      east.updateFrom(gpad);
      south.updateFrom(gpad);
      west.updateFrom(gpad);
    }
    
    void play(IPlayerProxy myInstrument) {
      north.play(myInstrument);
      east.play(myInstrument);
      south.play(myInstrument);
      west.play(myInstrument);
    }
    
    void draw() {
      pushMatrix();
        translate(width/2,height/2);
        color fc = color(100,200,255);
        color fc2 = color(200,240,255);
        color sc = color(255);
        
        north.draw(0,-80, north.isOn()?fc:fc2,sc);
        east.draw(80,0,east.isOn()?fc:fc2,sc);
        south.draw(0,80,south.isOn()?fc:fc2,sc);
        west.draw(-80,0,west.isOn()?fc:fc2,sc);

        addParticle(north,0,-80,fc);
        addParticle(east,80,0,fc);
        addParticle(south,0,80,fc);
        addParticle(west,-80,0,fc);
        ps.run();

      
      popMatrix();
      
    }

    void addParticle(SingleNoteControlProxy p, int x, int y, color c) {
      if (p.isOn()) {
        ps.addParticle(x,y,c);
      }
    }
}
