interface IControlProxy {
  void updateFrom(ControlDevice cd);
}

class SliderPairControl implements IControlProxy {
  public float x, y;
  String xParamId, yParamId;
  
  SliderPairControl(String xp, String yp) {
    xParamId = xp;
    yParamId = yp;
    x =0;
    y =0;
  }

  void updateFrom(ControlDevice cd) {
    set(gpad.getSlider(xParamId).getValue(), gpad.getSlider(yParamId).getValue()); 
  }
  
  void set(float ex, float wy) {
    x = ex;
    y = wy;
  }
  
  float getXfor(float min, float max) {
    return map(x,-1,1, min, max);
  }
  
  float getYfor(float min, float max) {
    return map(y,-1,1, min, max);
  }
  
  int getDX() {
    if (getXfor(-1,1) < -0.2) { return -1; }
    if (getXfor(-1,1) > 0.2) { return 1; }
    return 0;
  }
  
  int getDY() {
    if (getYfor(-1,1) < -0.2) { return -1; }
    if (getYfor(-1,1) > 0.2) { return 1; }
    return 0;
  }
  
}

enum Direction {
  NONE,NORTH,NORTHEAST,EAST,SOUTHEAST,SOUTH,SOUTHWEST,WEST,NORTHWEST
}

class HatControl implements IControlProxy {
  public Direction d;
  
  void updateFrom(ControlDevice gpad) {
    switch ((int)gpad.getHat("HAT").getValue()) {
       case 1 : d = Direction.NORTHWEST; break;
       case 2 : d = Direction.NORTH; break;
       case 3 : d = Direction.NORTHEAST; break;
       case 4 : d = Direction.EAST; break;
       case 5 : d = Direction.SOUTHEAST; break;
       case 6 : d = Direction.SOUTH; break;
       case 7 : d = Direction.SOUTHWEST; break;
       case 8 : d = Direction.WEST; break;
       default:
         d = Direction.NONE; 
    }
  }
  
  Direction getDirection() {
    return d; 
  }
  
}



class ButtonControl implements IControlProxy {
   boolean down=false;
   int x, y;
   color c;
   String paramId;
   boolean pressed=false;
   boolean released=false;
   
   ButtonControl(String pId, int ex, int wy, color ac) {
     paramId = pId;
     x = ex;
     y = wy;
     c = ac;
   }
   
   void set(boolean d) {
      if (d && !down) {
         pressed = true;
         released = false;
         down = true;
         return;
      }
      if (!d && down) {
        released = true;
        pressed = false;
        down = false;
        return;
      }
      if (d && down) {
        pressed = false;
        released = false;
        down = d;
        return;
      }
      
      down = d;
      pressed = false;
      released = false;
   }
     
   void updateFrom(ControlDevice gpad) {
     set( gpad.getButton(paramId).pressed() );
   }
   
   boolean isDown() {
     return down;
   }
   
   boolean isPressed() {
     return pressed;
   }
   
   boolean isReleased() {
     return released;
   }
   
} 

class ToggleSwitch extends ButtonControl {
   boolean _toggledOn = false;
   boolean _toggledOff = false;
   boolean state = false;
   
   ToggleSwitch(String pId,int ex, int wy, color ac) {
     super(pId,ex,wy,ac);
   }
   
   void set(boolean d) {
     if (d == down) {
       return;
     }
     down = d;

     if (d == true) {
       _toggledOn = true;
       state = !true;
       return;
     }
     
     _toggledOff = true;    
   }

   void reset() {
     down = false;
     _toggledOn = false;
     _toggledOff = false;
   }
   
   boolean toggledOn() {
     return _toggledOn; 
   }
   
   boolean toggledOff() {
     return _toggledOff;
   }
}

class ButtonDrum implements IControlProxy {
  ToggleSwitch ts;
  String controllerParamId;
  int midiNote;
  int vol = 0;
  int channel, param;
  
  ButtonDrum(String paramId, int chan, int note, int x, int y, color c) {
    channel = chan;
    controllerParamId = paramId;
    midiNote = note;
    ts = new ToggleSwitch(controllerParamId,x,y,c);
  }

  
  void set(boolean b) {
    ts.set(b);
  }
  
  boolean toggledOn() { return ts.toggledOn(); }
  boolean toggledOff() { return ts.toggledOff(); }
  
  void play(IPlayerProxy myInstrument) {
    if (ts.toggledOn()) {
      myInstrument.sendNote(2,midiNote,100) ;
    }

    ts.reset();
    throttler.step();
  }
  
  void draw(int x, int y, color fillCol, color strokeCol) {
    pushStyle();
    fill(fillCol);
    stroke(strokeCol);
    ellipse(x,y,30,30);
    popStyle();
  }
  
  void updateFrom(ControlDevice cd) {
    set(gpad.getButton(controllerParamId).pressed());
  }

}
