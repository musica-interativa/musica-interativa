
import org.gamecontrolplus.gui.*;
import org.gamecontrolplus.*;
import net.java.games.input.*;

import themidibus.*; 

// The Instrument (MIDI connection)
MidiBus myBus; 
IPlayerProxy myInstrument;
Throttler throttler = new Throttler();

// The Gamepad 
ControlIO control;
Configuration config;
ControlDevice gpad;

// The Game
ImageCollection backs;
AgentCollection agents;
Game game;

void setup() {
  size(1200,1200,P3D);

  game = makeTestGame();
  backs = game.getBackgrounds();
  agents = game.getAgents();
  
  control = ControlIO.getInstance(this);
  gpad = control.getMatchedDevice("gamepadmidi");
  if (gpad == null) {
    println("No suitable device configured");
    System.exit(-1); // End the program NOW!
  }
  
  MidiBus.list();
  myBus = new MidiBus(this, -1, "LoopBe Internal MIDI"); // Create a new MidiBus with no input device and LoopBe as output.
  //myBus = new MidiBus(this, -1, "01. Internal MIDI"); // Create a new MidiBus with no input device and LoopBe as output.
  myBus.clearInputs();
  
  myInstrument = new MidiProxy(myBus);
  
}



// MAIN CODE
///////////////////////////////////////////////////////////////////////////

/*
//SimpleJoystickCCAgent leftJoy = new SimpleJoystickCCAgent ("LEFT_X","LEFT_Y",   "TRIG1", color(0,0,255,100), 2, 5, 6, 50, 50);
//IAgent rightJoy = new SimpleJoystickNotesAgent("RIGHT_X","RIGHT_Y", "TRIG2", color(255,0,100,100), 20, 2, 50, 150);
IAgent leftJoy = new StableJoystickNotesAgent ("LEFT_X","LEFT_Y",   "TRIG3", "TRIG1", color(0,0,255,100), 25, 5);
//IAgent cta = new CardinalTogglesAgent("B_NORTH","B_EAST","B_SOUTH","B_WEST");
//IAgent hat = new HatNotesAgent("TRIG4", color(100,255,100), 40, 3);
*/


void draw() {
  backs.draw();  
  agents.updateFrom(gpad);
  agents.draw();
  agents.play(myInstrument);
}
