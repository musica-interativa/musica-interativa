class BGImage {
  PImage im;
  float scaler;
  float m;
  float rc;
  
  BGImage(String imgName, float startScaler) {
     scaler = startScaler;
     im = loadImage(imgName);
     m = 1.0 + random(100)/4000.0;
     rc = random(8);   
  }
  
  void draw() {
    imageMode(CENTER);
    pushMatrix();
    pushStyle();
       translate(width/2,height/2);
       rotate(frameCount/(127.0+rc));
       //rotateY(frameCount/5);
       tint(255, 10);
       image(im,0,0,width*scaler,height*scaler);
    popStyle();
    popMatrix();
    scaler=scaler*m; // 1.01;
    if (scaler > 150) { scaler = 0.01; }
  }
  
}

class ImageCollection {
  ArrayList<BGImage> images = new ArrayList<BGImage>();
  
  void add(String imgName, float startScaler) {
    images.add(new BGImage(imgName,startScaler));
  }
  
  void draw() {
    for (BGImage im : images) {
      im.draw();
    }
  }
}
