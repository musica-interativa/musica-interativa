# código-fonte do projeto musica-interatica

### Projeto 01: Aorist Theremin

_O nome Aorist é uma referência à tecnologia "Aorist Rods" do Guia do
Mochileiro das Galáxias_ [1][aorist1] [2][aorist2].

Este projeto de instrumento musical construído a base de Arduino e sensor de
distância ultrasônico HC-SR04 envia os sinais captados da distância da mão do
usuário para um software escrito em SuperCollider, um ambiente de programação
musical cliente-servidor.

A distância entre o sensor HC-SR04 e a mão de quem toca o instrumento é
utilizado como parâmetros de entrada para os algoritmos responsáveis por
sintetizar sons, programados com SuperCollider, além do sensor de distância o
instrumento Aorist Theremin conta também com 5 botões para seleção de
sintetizadores distindos, podendo ser combinados entre sí em qualquer ordem.

Cada sintetizador é implementado em linguagem de programaçao SuperCollider e
recebem parâmetros indicando a distância da mão do usuário ao sensor
ultrasônico e também o movimento feito pela mão, quanto mais movimentos com a
mão maior o segundo argumento chamado de balanço ou movimento (shake, em
inglês) passado ao sintetizador.

O código-fonte utilizado no Arduino pode ser consultado no link abaixo:

* https://gitlab.com/musica-interativa/musica-interativa/blob/master/sketchbook/one_sensor_hcsr04_arduino/

Os sintetizadores e códigos responsáveis pro processar as entradas em
SuperCollider pode ser consultados em:

* https://gitlab.com/musica-interativa/musica-interativa/tree/master/supercollider

[aorist1]: https://hitchhikers.fandom.com/wiki/Aorist_Rods
[aorist2]: https://sites.google.com/site/h2g2theguide/Index/a/767111
