"arduino-hcsr04-init.sc".loadRelative

s.record;

(
// instrumento 01
x = SynthDef("arduino_synth", {
  arg freq = 440, modfreq = 1, amp = 0.3, attack = 0.5, dur = 0.2, pos = 0.5;
  var carrier, modulator, env;
  modulator = LFSaw.kr(modfreq).range(0.5, 0.7);
  carrier = LFSaw.ar(freq: freq, mul: modulator);
  env = Env.perc(attackTime: attack, releaseTime: dur - attack, level: amp).kr(2);
  carrier = carrier * env;
  Out.ar(0, Pan2.ar(carrier, pos))
}).add;

// instrumento 05
y = SynthDef(\supermandolin, {|out, sustain=1, pan, accelerate, freq, detune=0.2 |
	var env = EnvGen.ar(Env.linen(0.002, 0.996, 0.002, 1,-3), timeScale:sustain, doneAction:2);
	var sound = Decay.ar(Impulse.ar(0,0,0.2), 0.1*(freq.cpsmidi)/69) * WhiteNoise.ar;
	var pitch = freq * Line.kr(1, 1+accelerate, sustain);
	sound = CombL.ar(sound, 0.05, pitch.reciprocal*(1-(detune/100)), sustain)
	+ CombL.ar(sound, 0.05, pitch.reciprocal*(1+(detune/100)), sustain);
	OffsetOut.ar(out, DirtPan.ar(sound, 2, pan, env))
}).add;

// instrumento 06
z = SynthDef(\superpwm, {|out, speed=1, decay=0, sustain=1, pan, accelerate, freq,
	voice=0.5, semitone=12, resonance=0.2, lfo=1, pitch1=1|
	var env = EnvGen.ar(Env.pairs([[0,0],[0.05,1],[0.2,1-decay],[0.95,1-decay],[1,0]], -3), timeScale:sustain, doneAction:2);
	var env2 = EnvGen.ar(Env.pairs([[0,0.1],[0.1,1],[0.4,0.5],[0.9,0.2],[1,0.2]], -3), timeScale:sustain/speed);
	var basefreq = freq * Line.kr(1, 1+accelerate, sustain);
	var basefreq2 = basefreq / (2**(semitone/12));
	var lfof1 = min(basefreq*10*pitch1, 22000);
	var lfof2 = min(lfof1 * (lfo + 1), 22000);
	var sound = 0.7 * PulseDPW.ar(basefreq) * DelayC.ar(PulseDPW.ar(basefreq), 0.2, Line.kr(0,voice,sustain)/basefreq);
	sound = 0.3 * PulseDPW.ar(basefreq2) * DelayC.ar(PulseDPW.ar(basefreq2), 0.2, Line.kr(0.1,0.1+voice,sustain)/basefreq) + sound;
	sound = MoogFF.ar(sound, SinOsc.ar(basefreq/32*speed, 0).range(lfof1,lfof2), resonance*4);
	sound = MoogFF.ar(sound, min(env2*lfof2*1.1, 22000), 3);
	sound = sound.tanh*5;
	OffsetOut.ar(out, DirtPan.ar(sound, 2, pan, env));
}).add;

r = Routine({ inf.do {
	if (m == 1, // button 1
		{ // true
			Synth("arduino_synth", [
				\freq, 380 + d,
				\modfreq, (k * 2) / 100,
				\amp, k.linlin(0, 100, 0.1, 1),
			])
		},
		{ x.free }  // false
	);
	if (n == 1, // button 2
		{ // true
			Synth.new(\supermandolin, [
				\freq, d.linlin(0, 60, 800, 300),
				\sustain, k.linlin(0, 100, 1, 10),
				\detune, k.linlin(0, 100, 0.2, 1),
			])
		},
		{ y.free } // false
	);
	if (o == 1, // button 3
		{ // true
			Synth.new(\superpwm, [
				\freq, d.linlin(0, 60, 400, 100),
				\sustain, k.linlin(0, 100, 0.2, 5),
				\semitone, k.linlin(0, 100, 0, 13),
				\lfo, d.linlin(0, 60, 0, 9),
				\voice, k,
			])
		},
		{ z.free }  // false
	);
	0.5.wait;
}}).play;
)

(
x.free;
y.free;
z.free;
r.stop;
)

s.stopRecording;

"arduino-hcsr04-stop.sc".loadRelative