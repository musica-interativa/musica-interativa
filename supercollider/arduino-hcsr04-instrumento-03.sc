"arduino-hcsr04-init.sc".loadRelative

( // instrumento 03: Decimator
z = {
	Decimator.ar(
		SinOsc.ar([420 - In.kr(~distance), 424 - In.kr(~distance)], 0, 0.2),
		In.kr(~distance) * 10,
		In.kr(~shake) / 10

    )
}.play;
)

z.release;

"arduino-hcsr04-stop.sc".loadRelative