( // loop to read Arduino serial from HCSR-04 sensor
  // also, read on/off buttons
s.boot;
s.waitForBoot;
~distance = Bus.control(s, 1);
~shake = Bus.control(s, 1);
~button1 = Bus.control(s, 1);
~button2 = Bus.control(s, 1);
~button3 = Bus.control(s, 1);
~port = SerialPort("/dev/ttyACM0", baudrate: 115200); // crtscts: true);
~loop = Routine({ inf.do{
	d = ~port.read;
	k = ~port.read;
	m = ~port.read; // button 1
	n = ~port.read; // button 2
	o = ~port.read; // button 3
	("distance: " + d).postln;
	("shake: " + k).postln;
	("button1: " + m).postln;
	("button2: " + n).postln;
	("button3: " + o).postln;
	~distance.set(d);
	~shake.set(k);
	~button1.set(m);
	~button2.set(n);
	~button3.set(o);
}}).play;
)