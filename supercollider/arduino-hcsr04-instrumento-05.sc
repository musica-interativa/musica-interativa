"arduino-hcsr04-init.sc".loadRelative

( // copied from SuperDirt libraries default-synth-extra.scd
	SynthDef(\supermandolin, {|out, sustain=1, pan, accelerate, freq, detune=0.2 |
		var env = EnvGen.ar(Env.linen(0.002, 0.996, 0.002, 1,-3), timeScale:sustain, doneAction:2);
		var sound = Decay.ar(Impulse.ar(0,0,0.1), 0.1*(freq.cpsmidi)/69) * WhiteNoise.ar;
		var pitch = freq * Line.kr(1, 1+accelerate, sustain);
		sound = CombL.ar(sound, 0.05, pitch.reciprocal*(1-(detune/100)), sustain)
		+ CombL.ar(sound, 0.05, pitch.reciprocal*(1+(detune/100)), sustain);
		OffsetOut.ar(out, DirtPan.ar(sound, 2, pan, env))
	}).add
);

(
~control = Routine.new({
	{
		Synth.new(\supermandolin, [
			\freq, d.linlin(0, 60, 800, 300),
			\sustain, k.linlin(0, 100, 1, 10),
			\detune, k.linlin(0, 100, 0.2, 1),
		]);
		0.2.wait;
	}.loop;
}).play;
)

~control.stop;

"arduino-hcsr04-stop.sc".loadRelative