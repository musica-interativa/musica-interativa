(
~control = Routine.new({
	{
		Synth.new(\supergong, [
			\freq, d.linlin(0, 60, 800, 300),
			\sustain, d.linlin(0, 100, 1, 10),
		]);
		0.2.wait;
	}.loop;
}).play;
)

(
~control = Routine.new({
	{
		Synth.new(\superhex, [
			\freq, d.linlin(0, 60, 800, 300),
			\sustain, d.linlin(0, 100, 1, 10),
		]);
		0.2.wait;
	}.loop;
}).play;
)

\super808
\superpwm

(
SynthDef.new(\saw, {
	arg cutoff = 1000, mul = 0.2, add = 1, freq = 400;
	var sig;
	sig = Saw.ar([30, 60]);
	//cutoff.lag(0.02)
	sig = RLPF.ar(sig, freq, mul, add);
	Out.ar(0, sig);
}).add;
~synth = Synth(\saw);
)


~synth = Synth(\supermandolin);

(
~control = Routine.new({
	{
		~synth.set(\freq, d.linlin(0, 60, 2000, 300));
		0.01.wait;
	}.loop;
}).play;
)


(
~control = Routine.new({
	{
		~synth.set(\cutoff, exp(d));
		~synth.set(\freq, d.linlin(0, 60, 2000, 300));
		~synth.set(\mul, (k / 10) + 0.5);
		~synth.set(\add, (k / 20) + 0.5);
		0.01.wait;
	}.loop;
}).play;
)

