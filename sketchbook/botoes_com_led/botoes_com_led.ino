/*
autor: joenio
data: novembro 2018
circuito feito com base nos tutoriais abaixo:
   https://www.instructables.com/id/How-to-use-a-Push-Button-Arduino-Tutorial/
   https://www.electroschematics.com/8964/turn-on-led-button-arduino/
*/


/* Learn to use pushbutton (button switch) with Arduino - Tutorial
   More info and circuit schematic: http://www.ardumotive.com/arduino-tutorials/category/button
   Dev: Michalis Vasilakis / Date: 19/10/2014  
   UPDATED 2/6/2016 - LED to Arduino pin 3 and button to Arduino pin 4*/

const int NUMBER_OF_BUTTONS = 3;
const int buttonsPin[] = {8, 6, 4};
const int ledsPin[] =  {9, 7, 5};
int buttonsState[] = {0, 0, 0};
int flags[] = {-1, -1, -1};
boolean flag[] = {false, false, false};
int count[] = {0, 0, 0};

void setup() {
  //Input or output?
  for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
    pinMode(ledsPin[i], OUTPUT);      
    pinMode(buttonsPin[i], INPUT_PULLUP);
  }
  Serial.begin(115200);
}

void loop(){
  //Read button state (pressed or not pressed?)
  for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
    buttonsState[i] = digitalRead(buttonsPin[i]);
    if (buttonsState[i] == HIGH) { // botao pressionado
      count[i] = count[i] + 1;
    }
    else if (buttonsState[i] == LOW) { // botao solto
      if (count[i] > 3) {
        flag[i] = not flag[i];
        count[i] = 0;
      }
    }
    //flag[i] = readToggleButtonState(buttonOnePin);
    if (buttonsState[i] == LOW) { // botao solto
      // turn led on!
      if (flag[i]){
        digitalWrite(ledsPin[i], HIGH);
      }
      // turn led off!
      else {
        digitalWrite(ledsPin[i], LOW);
      }
    }
  }
  delay(100);
  return;
}
