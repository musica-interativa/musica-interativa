const int MAIN_LOOP_DELAY = 150;
const boolean DEBUG = false;

struct HCSR_04 {
  const int TRIG = 9;
  const int ECHO = 8;
  long duration = 0;
  int distance = 0;
  const int MAX_DISTANCE = 60;
  int previous_distance = 0;
  int delta = 0;
  int shake = 50; // values: [0 ~ 100], default value: any arbitrary initial value greather than 0
  const boolean SEND_CONTINUALLY = true; // if false send only distinct values
} hc;

struct BUTTONS {
  const int NUMBER_OF_BUTTONS = 3;
  const int buttonsPin[3] = {6, 4, 2};
  const int ledsPin[3] =  {7, 5, 3};
  int buttonsState[3] = {0, 0, 0};
  int flags[3] = {-1, -1, -1};
  boolean flag[3] = {false, false, false};
  int count[3] = {0, 0, 0};
} btn;

void setup() {
  // init HCSR 04 sensor
  pinMode(hc.TRIG, OUTPUT);
  pinMode(hc.ECHO, INPUT);
  // init buttons and leds
  for (int i = 0; i < btn.NUMBER_OF_BUTTONS; i++) {
    pinMode(btn.ledsPin[i], OUTPUT);      
    pinMode(btn.buttonsPin[i], INPUT_PULLUP);
  }
  Serial.begin(115200);
}

void loop() {
  // read button state (pressed or not pressed?)
  for (int i = 0; i < btn.NUMBER_OF_BUTTONS; i++) {
    btn.buttonsState[i] = digitalRead(btn.buttonsPin[i]);
    if (btn.buttonsState[i] == HIGH) { // botao pressionado
      btn.count[i] = btn.count[i] + 1;
    }
    else if (btn.buttonsState[i] == LOW) { // botao solto
      if (btn.count[i] > 3) {
        btn.flag[i] = not btn.flag[i];
        btn.count[i] = 0;
      }
    }
    //flag[i] = readToggleButtonState(buttonOnePin);
    if (btn.buttonsState[i] == LOW) { // botao solto
      // turn led on!
      if (btn.flag[i]){
        digitalWrite(btn.ledsPin[i], HIGH);
      }
      // turn led off!
      else {
        digitalWrite(btn.ledsPin[i], LOW);
      }
    }
  }

  // read HCSR 04 sensor
  // Clears the trigPin
  digitalWrite(hc.TRIG, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(hc.TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(hc.TRIG, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  hc.duration = pulseIn(hc.ECHO, HIGH);
  // Calculating the distance
  hc.distance = hc.duration * 0.034 / 2;
  int delta = abs(hc.distance - hc.previous_distance);
  if (hc.distance > hc.MAX_DISTANCE || delta > hc.MAX_DISTANCE) {
    hc.distance = hc.previous_distance;
  }
  hc.delta = abs(hc.distance - hc.previous_distance);
  if (hc.delta == 0) {
    hc.shake = hc.shake - 1;
  }
  else if (hc.delta <= 3) {
    hc.shake = hc.shake; // this line is just for better readabilitty
  }
  else {
    hc.shake = hc.shake + 5; // the increase rate is greather than decrease rate
  }
  if (hc.shake < 0) {
    hc.shake = 0;
  }
  else if (hc.shake > 100) {
    hc.shake = 100;
  }
  if (hc.SEND_CONTINUALLY || hc.distance != hc.previous_distance) {
    unsigned char buf[2];
    buf[0] = (unsigned char) hc.distance;
    buf[1] = (unsigned char) hc.shake;
    if (not DEBUG) {
      Serial.write(buf, 2); // send 2 bytes: distance, shake
    }
    else {
      Serial.print("distance: ");
      Serial.println(hc.distance);
      Serial.print("shake: ");
      Serial.println(hc.shake);
    }
    hc.previous_distance = hc.distance;
    // send buttons to serial
    unsigned char buf_btn[btn.NUMBER_OF_BUTTONS];
    for (int i = 0; i < btn.NUMBER_OF_BUTTONS; i++) {
      buf_btn[i] = btn.flag[i];
    }
    if (not DEBUG) {
      Serial.write(buf_btn, btn.NUMBER_OF_BUTTONS); // send NUMBER_OF_BUTTONS bytes
    }
    else {
      for (int i = 0; i < btn.NUMBER_OF_BUTTONS; i++) {
        Serial.print("button[");
        Serial.print(i);
        Serial.print("]: ");
        Serial.println(btn.flag[i]);
      }
    }
    
  }
  delay(MAIN_LOOP_DELAY);
}
