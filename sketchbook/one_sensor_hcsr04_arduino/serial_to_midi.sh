#!/bin/sh

PORT=/dev/ttyACM0

while read -r line < $PORT; do
  echo $line
  echo "{ SinOsc.ar(440, 0, 0.2) }.play;" > /tmp/sensor_hcsr04-pid$$.sc
  sclang /tmp/sensor_hcsr04-pid$$.sc
done
