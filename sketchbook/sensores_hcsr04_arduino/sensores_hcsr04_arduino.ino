// https://github.com/gamgine/HCSR04-ultrasonic-sensor-lib
#include <HCSR04.h>

// ???
#include "MIDIUSB.h"

// number of sensors
const int NSENSORS = 2;

// initialization class HCSR04 (trig pin, [echo pins], number of sensors)
HCSR04 hc(2, new int[NSENSORS]{4, 6}, NSENSORS);

// maximum acceptable difference between previous and current sensor value
const int ACCEPTABLE_DELTA = 100;

// previous and current sensor collected values
int previous_value[NSENSORS]; // = {0,0};
int current_value[NSENSORS]; // = {0,0};

// midi notes
const int A = 48;
const int B = 52;
const int C = 55;

void setup() {
  Serial.begin(115200);
  // calibrate sensors, just read distance values first time
  for (int count = 5; count > 0; count--) {
    for (int i = 0; i < NSENSORS; i++ ) {
      current_value[i] = previous_value[i] = hc.dist(i);
    }
    delay(50);
  }
}

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

void play_midi(int note, int velocity) {
  //Serial.println("Sending note on");
  noteOn(0, note, velocity);   // Channel 0, middle C, normal velocity
  MidiUSB.flush();
  //Serial.println("Sending note off");
  noteOff(0, A, velocity);
  noteOff(0, B, velocity);
  noteOff(0, C, velocity);
  MidiUSB.flush();
}

void loop() {
  for (int i = 0; i < NSENSORS; i++) {
    current_value[i] = hc.dist(i);
    int delta = current_value[i] - previous_value[i];
    if (delta < 0) {
      delta = delta * -1;
    }
    if (delta > ACCEPTABLE_DELTA) {
      current_value[i] = previous_value[i];
    }
    Serial.print(current_value[i]);
    if (i < NSENSORS - 1) {
      Serial.print(",");
    }
    previous_value[i] = current_value[i];
    delay(50);
  }
  Serial.println("");
  for (int i = 0; i < NSENSORS; i++) {
    if (current_value[i] > 20 and current_value[i] != -1 ) {
      play_midi(A, 64);
    }
    if (current_value[i] <=20 and current_value[i] >= 10 and current_value[i] != -1) {
      play_midi(B, 64);
    }
    if (current_value[i] < 10 and current_value[i] != -1) {
      play_midi(C, 64);
    }
  }
  delay(300);
}

