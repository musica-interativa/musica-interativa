

const boolean DEBUG = true; // enable verbose mode
const int LOOP_DELAY = 10; // main loop delay

class LDRSensor {
   public:

   int pin, mmin, mmax, calib;
   boolean started = false;
   
   void begin(int p) {
     pin = p;
     pinMode(pin, INPUT);
     started = true;
     recalibrate();
   }
   
   void recalibrate() {
     calib = 10000;
     mmin = 9999;
     mmax = -9999;
   }

   int get() {
     if (!started) { return 0; } // nao temos um numero do pin
     
     int v = analogRead(pin);
     if (calib > 0) {
       calib--;
       if (v < mmin) { mmin = v; }
       if (v > mmax) { mmax = v; }
     }
     return map(v,mmin,mmax,0,127);
   }
};

class ControlButton {
  public:
  int pin;
  int buttonState;
  int dedupe;
  
  void begin(int p) {
    pin = p;
    pinMode(p,INPUT);
  }

  void check() {
    if (dedupe > 1) { 
      dedupe--;
      return;       
    }
    int dr = digitalRead(pin);
    if (dr != buttonState) {
      buttonState = dr;
      dedupe = 500;
    }
  }

  boolean buttonDown() { 
    return buttonState == HIGH; 
  }
  
};


LDRSensor sensors[4];
ControlButton b1;
ControlButton b2;

void setup() {
  
  Serial.begin(115200); // start the serial communication
  if (DEBUG) {
    Serial.println("");
  }  
  sensors[0].begin(2);
  sensors[1].begin(3);
  sensors[2].begin(4);
  sensors[3].begin(5);

  b1.begin(0); // setup button pins
  b2.begin(1);
}

void loop() {
  for (int i=0; i<4;i++) {
    int v = sensors[i].get();
    if (DEBUG) {
      Serial.print(v);
      Serial.print(", ");
    }
  }
  Serial.println("");
  delay(LOOP_DELAY);
}
