/*
Sensor de luz com LDR para construção do Illumaphone
Autores: Deivi Kuhn, Joenio Costa, Phil Jones
Projeto: Musica-Interativa - https://gitlab.com/musica-interativa/
Criado em: 17 de agosto 2018
Licença: GPLv3
*/

const boolean DEBUG = false; // enable verbose mode
const int LOOP_DELAY = 10; // main loop delay
const int sensor1 = 2; // Sensor LDR no pino analogico 2
const int sensor2 = 3; // Sensor LDR no pino analogico 3
const int sensor3 = 4; // Sensor LDR no pino analogico 4
const int sensor4 = 5; // Sensor LDR no pino analogico 5
int autodetected_threshold_inferior;
int autodetected_threshold_superior;

void _debug(String msg, int value = 0) {
  if (DEBUG) {
    Serial.print("[DEBUG] ");
    Serial.print(msg);
    Serial.println(value);
  }
}

// return the minumim value of an array of integers
// array_size is the lenght of the values[] array
int minValue(int values[], int array_size) {
  int min_value = values[0]; // initialize the min_value with first item of the array
  for (int i = 0; i < array_size; i++) {
    min_value = min(min_value, values[i]);
    //_debug("minValue(): min_value = ", min_value);
  }
  return min_value;
}

// return the maximum value of an array of integers
// array_size is the lenght of the values[] array
int maxValue(int values[], int array_size) {
  int max_value = values[0]; // initialize the max_value with first item of the array
  for (int i = 0; i < array_size; i++) {
    max_value = max(max_value, values[i]);
    //_debug("maxValue(): max_value = ", max_value);
  }
  return max_value;
}

// time_count is the duration used to calibrate min and max values
// detected by sensors
void autocalibrateThresholds(int time_count) {
  int interval = 100;
  _debug("autocalibrateThresholds(): auto calibrating thresholds, duration = ", time_count);
  if (DEBUG) {
    Serial.print("[DEBUG] autocalibrateThresholds(): ");
  }
  while (time_count > 0) {
    time_count = time_count - interval;
    int values[] = {autodetected_threshold_inferior, autodetected_threshold_superior, analogRead(sensor1), analogRead(sensor2), analogRead(sensor3), analogRead(sensor4)};
    autodetected_threshold_inferior = minValue(values, 6);
    autodetected_threshold_superior = maxValue(values, 6);
    if (DEBUG) {
      Serial.print(".");
    }
    delay(interval);
  }
  if (DEBUG) {
    Serial.println(".");
  }
  _debug("autocalibrateThresholds(): autodetected threshold inferior = ", autodetected_threshold_inferior);
  _debug("autocalibrateThresholds(): autodetected threshold superior = ", autodetected_threshold_superior);
}

void setup() {
  pinMode(sensor1, INPUT);
  pinMode(sensor2, INPUT);
  pinMode(sensor3, INPUT);
  pinMode(sensor4, INPUT);
  Serial.begin(115200); // start the serial communication
  if (DEBUG) {
    Serial.println("");
  }
  int values[] = {analogRead(sensor1), analogRead(sensor2), analogRead(sensor3), analogRead(sensor4)};
  autodetected_threshold_inferior = minValue(values, 4); // initialize threshold inferior value
  autodetected_threshold_superior = maxValue(values, 4); // initialize threshold superior value
  autocalibrateThresholds(5000); // calibration time = 5000
}

int readNormalizedSensorValue(int pin) {
  // o valor possivel lido pelo sensor LDR fica entre 0 e 1023
  // mas queremos limitar e normalizar entre 0 e 127
  return map(analogRead(pin), autodetected_threshold_inferior, autodetected_threshold_superior, 0, 127);
}

void loop() {
  int sensor1_value = readNormalizedSensorValue(sensor1);
  int sensor2_value = readNormalizedSensorValue(sensor2);
  int sensor3_value = readNormalizedSensorValue(sensor3);
  int sensor4_value = readNormalizedSensorValue(sensor4);
  // imprime o valor lido dos sensores no monitor serial no seguinte formato: 000,000,000,000
  Serial.print(sensor1_value);
  Serial.print(",");
  Serial.print(sensor2_value);
  Serial.print(",");
  Serial.print(sensor3_value);
  Serial.print(",");
  Serial.println(sensor4_value);
  delay(LOOP_DELAY);
}
