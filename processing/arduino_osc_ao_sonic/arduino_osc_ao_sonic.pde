import processing.serial.*;

import oscP5.*;
import netP5.*;

Serial myPort;

OscP5 oscP5;
NetAddressList addresses = new NetAddressList();    

boolean isPlaying = false;

void add(String address, int port) {
  addresses.add(  new NetAddress(address, port) );
}


void setup() {
  size(100,100);
  printArray(Serial.list());
  if (Serial.list().length > 0) {
    myPort = new Serial(this, Serial.list()[0], 115200);
    //myPort = new Serial(this, Serial.list()[0], 9600);
  } else {
    println("No serial found");
  }
  oscP5 = new OscP5(this, 3333);
  add("localhost",4559);
}


float fv(Object o) { return ((Float)o).floatValue(); }

int iv(Object o) { return 30+(int)(fv(o) * 90); } 

int fix(Object o) { return ((Integer)o).intValue(); }

void send(String chan, float num) {
  OscMessage out = new OscMessage(chan);
  out.add(num);
  oscP5.send(out,addresses);  
}


void draw() {
  int a1,a2,a3,a4;
  while (myPort.available() > 0) {
    String inBuffer = myPort.readString(); 
    if (inBuffer != null) {
      try {
        inBuffer = inBuffer.trim();
        String[] parts = inBuffer.split(",");

        a1 = Integer.parseInt(parts[0]);
        a2 = Integer.parseInt(parts[1]);
        a3 = Integer.parseInt(parts[2]);
        a4 = Integer.parseInt(parts[3]);
        
      } catch (Exception e) {
        return;
      }
      println("a1:" + a1 + ", a2:" + a2 + ", a3:" + a3 + " a4:" + a4);
        send("/i1",a1); 
        send("/i2",a2);
        send("/i3",a3);
        send("/i4",a4);        
    }
  }
}
