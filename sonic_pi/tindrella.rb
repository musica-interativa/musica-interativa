use_bpm 85

live_loop :beat1 do
  sample :elec_flip, amp: 2, rate: 1.1
  sleep 0.5
  sleep 0.25
  sample :elec_cymbal, amp: 0.25
  sleep 0.25
end

live_loop :beat2 do
  7.times do
    sleep 0.5
    sample :drum_tom_lo_hard, rate: 2.7, amp: 0.4
    sleep 0.25
    sample :drum_tom_lo_hard, rate: 2.7, amp: 0.4
    sleep 0.25
  end
  sleep 0.5
  sample :drum_tom_lo_hard, rate: 2.7, amp: 0.5
  sleep 0.125
  sample :drum_tom_lo_hard, rate: 2.7, amp: 0.5
  sleep 0.125
  sample :drum_tom_lo_hard, rate: 2.7, amp: 0.5
  sleep 0.125
  sample :drum_tom_lo_hard, rate: 2.7, amp: 0.5
  sleep 0.125
end



c = :C0
cc = chord(:E0,'13')

c0 = [  chord(:G,"major")].ring

c1 = [ chord(:G,"major"),
       chord(:F,"major"),
       chord(:C,"major"),
       chord(:G,"major")
       ].ring

c2 = [  chord(:Ds,"major"),
        chord(:G,"major"),
        chord(:Ds,"major7"),
        chord(:G,"major"),
        chord(:Ds,"major"),
        chord(:G,"minor7"),
        chord(:Ds,"m11"),
        chord(:G,"major")].ring


with_synth :tech_saws do
  live_loop :y do
    
    with_fx :ixi_techno, mix: 0.5 do
      4.times do
        c = c0.tick(:z)
        cc = c
        play c, amp: 3
        sleep 0.5
        play c+24, amp: 2, attack: 0, decay: 0.2, release: 0
        sleep 1
        play c+12, amp: 2, attack: 0, decay: 0.2, release: 0
        sleep 0.5
      end
    end
    
    4.times do
      with_fx :level, amp: 0.7 do
        with_fx :krush do
          with_synth [:saw, :dtri, :beep].choose do
            4.times do
              c = c1.tick(:x)
              cc = c
              play c, amp: 3
              sleep 0.5
              play c+24, amp: 2, attack: 0, decay: 0.2, release: 0
              sleep 1
              play c+24, amp: 2, attack: 0, decay: 0.2, release: 0
              sleep 0.5
            end
          end
        end
      end
    end
    
    
    8.times do
      c = c2.tick(:w)
      cc = c
      play c, amp: 3
      sleep 0.5
      play c+24, amp: 2, attack: 0, decay: 0.2, release: 0
      sleep 1
      play c+12, amp: 2, attack: 0, decay: 0.2, release: 0
      sleep 0.5
    end
    
    sleep 8
    
    with_fx :ixi_techno, mix: 0.5 do
      4.times do
        cc = c
        c = c1.tick(:x)
        play c, amp: 3
        sleep 0.5
        play c+24, amp: 2, attack: 0, decay: 0.2, release: 0
        sleep 1
        play c+24, amp: 2, attack: 0, decay: 0.2, release: 0
        sleep 0.5
      end
    end
    
    24.times do
      cc = c
      c = c2.tick(:w)
      play c, amp: 3
      sleep 0.5
      play c+24, amp: 2, attack: 0, decay: 0.2, release: 0
      sleep 1
      play c+12, amp: 2, attack: 0, decay: 0.2, release: 0
      sleep 0.5
    end
    
    
  end
end


with_fx :level, amp: 0.8 do
  with_fx :wobble, phase: 0.25 do
    with_synth :subpulse do
      
      live_loop :z do
        sleep 1
        play c-12, amp: 3
        sleep 3
      end
    end
  end
end

with_fx :level, amp: 0.3 do
  with_fx :gverb, mix: 0.4 do
    with_fx :flanger, phase: 0.666 do
      with_synth :beep do
        live_loop :arp do
          sleep 8
          18.times do
            ccc = (cc.reverse)[0,2]
            ccc.each { | x |
              play x+12, attack: 0, decay: 0.05, release: 0
              sleep 0.25
            }
            sleep 0.25
            play ccc[0], attack: 0, decay: 0.05, release: 0
            
            sleep 1
          end
          sleep 96
        end
      end
    end
  end
end


