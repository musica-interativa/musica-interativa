
chord_types = {21 => "major7",
               22 => "minor7",
               23 => "m11"}

chord_type = 21
root = :C3
i1=0
i2=0
i3=0
i4=0


with_fx :level, amp: 0.4 do
  with_fx :lpf do |lp|
    with_fx :reverb do
      with_synth :prophet do
        live_loop :x do
          chord(root,chord_types[chord_type]).each { | n |
            control lp, cutoff: 130-i4
            play n, attack: 0.01, decay: 0.2, release: 0
            sleep 0.25
            play n+12, attack: 0.01, decay: 0.1, release: 0
            sleep 0.25
          }
        end
      end
    end
  end
end


live_loop :beat do
  sample :bd_gas, amp: 4, rate: 1.2
  sleep 0.25
  sleep 0.25
  sample :elec_blup, rate: 3
  sleep 0.25
  sleep 0.25
end

with_fx :slicer, mix: 0.75 do | sl |
  live_loop :beat2 do
    control sl, phase: [0.125,0.25,0.5,1,2,2].choose
    
    sample :loop_amen, beat_stretch: 2
    sleep 1
    sample :glitch_perc5
    sleep 1
  end
end



with_fx :compressor, amp: 0.5 do
  with_fx :reverb do
    with_fx :bitcrusher do |bc|
      with_synth :blade do
        live_loop :y do
          control bc, bits: [1,2,4,8,12,16].choose
          play root-12
          sleep 2
        end
      end
    end
  end
end



# CONTROLS .....................................................

live_loop :midiroot do
  use_real_time
  note, velocity = sync "/midi/uma25s/1/1/note_on"
  root = note
end

live_loop :midichord do
  use_real_time
  chord, vel = sync "/midi/uma25s/1/1/control_change"
  chord_type = chord
end

live_loop :osc1 do
  use_real_time
  i1 = sync "/osc/i1"
  i1 = i1[0].to_i
end

live_loop :osc2 do
  use_real_time
  i2 = sync "/osc/i2"
  i2 = i2[0].to_i
end

live_loop :osc3 do
  use_real_time
  i3 = sync "/osc/i3"
  i3 = i3[0].to_i
end

live_loop :osc4 do
  use_real_time
  i4 = sync "/osc/i4"
  i4 = i4[0].to_i
end
