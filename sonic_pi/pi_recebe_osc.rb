p1=0
p2=0
p3=0
p4=0
a1=1
a2=1
a3=1
a4=1

s = scale(:C0,:major)

def c(n,old,s)
  p = n[0].to_i()
  print s
  if s.include?(12+ p % 12) then
    while (p < 0)
      p = p + 12
    end
    
    if (p > 127) then
      p = 127
    end
    return p
  end
  return old
end

live_loop :p1 do
  use_real_time
  x = sync "/osc/i1"
  p1 = c(x,p1,s)
end

live_loop :p2 do
  use_real_time
  x = sync "/osc/i2"
  p2 = c(x,p2,s)
end

live_loop :p3 do
  use_real_time
  x = sync "/osc/i3"
  p3 = c(x,p3,s)
end

live_loop :p4 do
  use_real_time
  x = sync "/osc/i3"
  p4 = c(x,p4,s)
end

live_loop :a do
  sample :bd_boom, amp: 3
  sleep 0.5
  sample :drum_cowbell, amp: 0.1
  sleep 0.5
  sample :drum_snare_soft
  sleep 0.5
  sample :drum_cowbell, amp: 0.1
  sleep 0.5
end

with_fx :reverb do
  with_fx :echo do
    live_loop :b do
      with_synth :piano do
        play p1, amp: 0.4
        play p3, amp: 0.4
        
        sleep 0.25
        play p2, amp: 0.6
        play p4, amp: 0.4
        
        sleep 0.25
      end
    end
  end
end
